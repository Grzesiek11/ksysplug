#include <iostream>
#include <string>
#include <functional>
#include <vector>
#include <sstream>

// These are only needed by default plugins:
#include <random>
#include <regex>
#include <pstreams/pstream.h>



#define HELLO "ksysplug"
#define ENABLE_METADATA



class SensorType {

    public:
        enum Value {
            Integer,
            Float,
            ListView,
            Table,
            None
        };
        
        SensorType();
        SensorType(Value);

        bool operator==(const SensorType) const;
        bool operator!=(const SensorType) const;

        std::string str() const;
    
    private:
        Value value;
    
};

SensorType::SensorType() {
    this->value = None;
}

SensorType::SensorType(Value value) {
    this->value = value;
}

bool SensorType::operator==(const SensorType r) const {
    return value == r.value;
}

bool SensorType::operator!=(const SensorType r) const {
    return value != r.value;
}

std::string SensorType::str() const {
    switch (this->value) {
        
        case Integer:
            return "integer";
        
        case Float:
            return "float";
        
        case ListView:
            return "listview";
        
        case Table:
            return "table";
        
        default:
            return "";
        
    }
}


class Sensor {

    public:
        std::string getName();
        virtual SensorType getType();
        virtual std::string getMetadata() = 0;
        virtual std::string getValue() = 0;

    protected:
        std::string name;

};

std::string Sensor::getName() {
    return name;
}

SensorType Sensor::getType() {
    return SensorType::None;
}


class IntegerSensor : public Sensor {
    
    public:
        IntegerSensor(std::string, std::function<int()>, std::string, int, int);

        virtual SensorType getType();
        virtual std::string getMetadata();
        virtual std::string getValue();
    
    private:
        std::function<int()> value;

        std::string description;
        int min;
        int max;
    
};

IntegerSensor::IntegerSensor(std::string name, std::function<int()> value, std::string description, int min, int max) {
    this->name = name;
    this->value = value;
    this->description = description;
    this->min = min;
    this->max = max;
}

SensorType IntegerSensor::getType() {
    return SensorType::Integer;
}

std::string IntegerSensor::getMetadata() {
    std::stringstream ss;
    ss << description << '\t' << min << '\t' << max;
    return ss.str();
}

std::string IntegerSensor::getValue() {
    return std::to_string(value());
}


class FloatSensor : public Sensor {
    
    public:
        FloatSensor(std::string, std::function<double()>, std::string, double, double);

        virtual SensorType getType();
        virtual std::string getMetadata();
        virtual std::string getValue();
    
    private:
        std::function<double()> value;

        std::string description;
        double min;
        double max;
    
};

FloatSensor::FloatSensor(std::string name, std::function<double()> value, std::string description, double min, double max) {
    this->name = name;
    this->value = value;
    this->description = description;
    this->min = min;
    this->max = max;
}

SensorType FloatSensor::getType() {
    return SensorType::Float;
}

std::string FloatSensor::getMetadata() {
    std::stringstream ss;
    ss << description << '\t' << min << '\t' << max;
    return ss.str();
}

std::string FloatSensor::getValue() {
    return std::to_string(value());
}



int main() {
    std::vector<Sensor*> sensors;

    sensors.push_back(new IntegerSensor("random/integer", []()->int {
        std::random_device randomDevice;
        std::mt19937 generator(randomDevice());
        std::uniform_int_distribution<int> randomNumber(0.0f, 10.f);
        return randomNumber(generator);
    }, "Integer", 0, 10));

    sensors.push_back(new FloatSensor("random/float", []()->double {
        std::random_device randomDevice;
        std::mt19937 generator(randomDevice());
        std::uniform_real_distribution<double> randomNumber(0.0f, 10.f);
        return randomNumber(generator);
    }, "Float", 0.0f, 10.0f));

    sensors.push_back(new FloatSensor("ping/1.1.1.1", []()->double {
        redi::ipstream pingProcess("ping -c 1 1.1.1.1", redi::pstreams::pstdout);
        std::string line;
        while (std::getline(pingProcess.out(), line)) {
            std::smatch match;
            if (std::regex_match(line, match, std::regex("\\d+ bytes from .+: icmp_seq=\\d+ ttl=\\d+ time=(.*) ms"))) {
                return std::stod(match[1]);
            }
        }
        return 0;
    }, "1.1.1.1", 0.0f, 100.0f));

    sensors.push_back(new FloatSensor("ping/8.8.8.8", []()->double {
        redi::ipstream pingProcess("ping -c 1 8.8.8.8", redi::pstreams::pstdout);
        std::string line;
        while (std::getline(pingProcess.out(), line)) {
            std::smatch match;
            if (std::regex_match(line, match, std::regex("\\d+ bytes from .+: icmp_seq=\\d+ ttl=\\d+ time=(.*) ms"))) {
                return std::stod(match[1]);
            }
        }
        return 0;
    }, "8.8.8.8", 0.0f, 100.0f));

    sensors.push_back(new FloatSensor("ping/192.168.1.1", []()->double {
        redi::ipstream pingProcess("ping -c 1 192.168.1.1", redi::pstreams::pstdout);
        std::string line;
        while (std::getline(pingProcess.out(), line)) {
            std::smatch match;
            if (std::regex_match(line, match, std::regex("\\d+ bytes from .+: icmp_seq=\\d+ ttl=\\d+ time=(.*) ms"))) {
                return std::stod(match[1]);
            }
        }
        return 0;
    }, "192.168.1.1", 0.0f, 100.0f));

    std::cout << HELLO << '\n';
    while (1) {
        std::string command;

        std::cout << "ksysguardd> ";
        getline(std::cin, command);
        
        if (command == "monitors") {
            for (Sensor* sensor: sensors) {
                std::cout << sensor->getName() << '\t' << sensor->getType().str() << '\n';
            }
        } else {
            bool metadata = command.back() == '?';
            bool foundSensor = false;

            std::string sensorName = command;
            if (metadata) {
                sensorName.pop_back();
            }

            for (Sensor* sensor: sensors) {
                if (sensor->getName() == sensorName) {
                    #ifdef ENABLE_METADATA
                    if (metadata) {
                        std::cout << sensor->getMetadata() << '\n';
                    } else {
                    #endif
                        std::cout << sensor->getValue() << '\n';
                    #ifdef ENABLE_METADATA
                    }
                    #endif
                    foundSensor = true;
                    break;
                }
            }

            if (!foundSensor) {
                std::cout << "UNKNOWN COMMAND\n";
            }
        }
    }
}