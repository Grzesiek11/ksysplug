all:
	g++ src/main.cpp -o ksysplug

release:
	g++ -O3 -static -static-libstdc++ -static-libgcc src/main.cpp -o ksysplug
