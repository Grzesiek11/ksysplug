# ksysplug

**ksysplug** is a simple `ksysguardd` replacement (you can have both) with work-in-progress plugin system.

This basically means it allows you to add custom sensors to KDE's System Monitor (ksysguard).

## Installation and usage

1. Download the binary for Linux from [releases page](https://gitlab.com/grzesiek11/ksysplug/-/releases) or compile it yourself (see *Building* below).
2. Open ksysguard (System Monitor).
3. Create a new tab: *File* -> *New Tab...* (skip this and the next step if you already have a custom tab).
4. Set the tab options (defaults are good, name it in a descriptive way, *ksysplug* for example). You can change them later.
5. With the new tab selected, go to *File* -> *Monitor Remote Machine...*.
6. Enter a descriptive name (*ksysplug* for example) for ksysplug sensors in the *Host:* field.
7. Select *Custom command* connection type.
8. Enter the (absolute, but you can use `~`) path to ksysplug binary (including the binary itself) into the *Command:* field.
9. Now you can drag the custom sensors from the *Sensor Browser*.

## Sensors

### Default sensors

- Ping - parses the `ping` command output to monitor current ping to given IP adress. By default, there are 3 instances of it, pinging different adresses: `1.1.1.1`, `8.8.8.8` and `192.168.1.1`.
- Random - generates a random number (float or integer, depending on instance) between 0 and 10.

### Creating your own sensors

Currently, the plugin system is pretty limited, and you need to modify the source code (and rebuild it) to add/modify anything, but it works:

```cpp
sensors.push_back(new Sensor("name", lambda, "description", MIN, MAX));
```

You need to push a new sensor object into the `sensors` vector.

There are two sensor classes (outside of the base `Sensor` class) avaliable:

- `IntegerSensor` - returns integer values.
- `FloatSensor` - returns float values.

(ksysguard actually supports more than that, but I didn't implement them yet).

Both classes have similar constructors.

1. `std::string name` - ksysguard shows it in *Sensor Browser*. You can use slashes to make a tree structure.
2. `std::function<int()> value` or `std::function<double()> value` - Function returning the value.
3. `std::string description` - Shown under the sensor when it's placed on a grid. It shouldn't be required (it isn't by ksysguard), but I didn't implement optional features yet.
4. `int min` or `double min` - Minimum value. As above, should be optional, but isn't.
5. `int max` or `double max` - Maximum value. As above, should be optional, but isn't.

### The future

I started to implement a proper plugin system in the `plugins` branch. It will use Lua to allow dynamic plugin loading without recompiling anything. At the time I'm writing this, almost nothing is done yet.

## Building

It requires `g++` and `make`. Default plugins also require `pstreams` library.

Installation on Debian-based systems:

```sh
sudo apt install g++ make libpstreams-dev
```

Download the source, go into the folder containing the `Makefile` and enter the command:

```sh
make
```

Or (if you want a statically-linked, optimised binary):

```sh
make release
```
